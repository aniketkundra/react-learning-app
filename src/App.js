import logo from './logo.svg';
import './App.css';
import RootContainer from './RootContainer';


function App() {
  return (
    <div className="App">
      <RootContainer/>
    </div>
  );
}

export default App;
