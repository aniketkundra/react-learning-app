import React, { Component } from 'react'
import EmailInput from './Email/EmailInput'
import EmailVerifier from './Email/EmailVerifier'
import { } from "local-storage";
import SignupOption from './SignupOption';
import Phone from './Phone/Phone';
import PhoneVerifier from './Phone/PhoneVerifier';
import axios from 'axios';
import {ConfigHolder} from './config/config';
import Loader from './utils/Loader';
import PersonalDetails from './PersonalDetails/PersonalDetails'

class Container extends Component {
    constructor(props) {
        super(props)

        this.state = {
            screens: {},
            source: 'SELF_SIGN_UP_WEB',
            checkUser: true,
            emailOtp:'',
            screenConstants: {
                'SignupOption': SignupOption,
                'EmailInput': EmailInput,
                'EmailVerifier': EmailVerifier,
                'Phone': Phone,
                'PhoneVerifier': PhoneVerifier,
                'PersonalDetails': PersonalDetails
            },
            signupProcess: 'ALL',
            current: 0,
            userFields: {
                'email': '',
                'firstName': '',
                'lastName': '',
                'phone': ''
            },
            selectedScreen: '',
            childProps: {},
            enableLoader: true,
        }
    }

    customHeader = {
        headers: {
            "api-info": "V2|appVerson|deviceBrand|deviceModel|deviceScreenResolution|deviceOs|deviceOsVersion|deviceNetworkProvider|deviceNetworkType",

        }
    }

    componentDidMount(){
        axios.defaults.baseURL = ConfigHolder.getconfig().baseUrl
        axios.get('/util/getSignUpConfig').then(respone => {
            var tempScreens = new Map();
            for (const [key, value] of Object.entries(respone.data.signUpScreenFlow)) {
                var actualTemplates = []
                var values = value.forEach(element => {
                    actualTemplates.push(this.state.screenConstants[element])
                })
                tempScreens.set(key, actualTemplates)
            }
            this.setState({
                signupProcess: respone.data.signUpProcess,
                screens: tempScreens,
                childProps : this.getPropsForComp(),
                enableLoader: false
            })
        })
    }

    getPropsForComp() {
        var propsAttrs = {
            'SignupOption' : {'setSignUpProcess': this.setSignUpProcess.bind(this)},
            'EmailInput': { 'submitInputEmail': this.submitInputEmail.bind(this), 'email': this.state.userFields.email, 'setUserFields': this.setUserFields.bind(this) },
            'EmailVerifier': { 'emailOtp': this.state.emailOtp, 'validateOtpForEmail': this.validateOtpForEmail.bind(this)},
            'PersonalDetails': { 'userFields': this.state.userFields, 'moveToNextStep': this.moveToNext.bind(this), 'setUserFields': this.setUserFields.bind(this)}
        }  
        console.log('emailOTP in getmethod : ' +  this.state.emailOtp)
        return propsAttrs
    }

    submitInputEmail = event => {
        this.setState({ enableLoader: true })
        var dataObj = {
            'checkUser': this.state.checkUser,
            'source': this.state.source,
            'email': this.state.userFields.email
        }
        //api for send otp and based on result go to next screen
        axios.post('/moSignUp/sendOneTimePassword', dataObj, this.customHeader).then(response => {
            if (response.data.success) {
                this.moveToNext()
            }
        })
    }

    moveToNext = () => {
        this.setState({
            current: this.state.current + 1,
            enableLoader: false
        })
    }
    moveToPrevStep = (prevState) => {
        if(this.state.current === 0){
            this.setState({
                signupProcess: 'ALL'
            })
        }
        else{
            this.setState({
                current: this.state.current - 1,
                enableLoader: false
            })
        }
    }

    setSignUpProcess = (e) => {
        this.setState({
            signupProcess: e.target.value
        })
    }

    validateOtpForMobile =() => {
        var dataObj = {
            objectType: 'phone'
        }
    }

    validateOtpForEmail =(emailOtp) => {
        var dataObj = {
            objectType: 'email',
            email :  this.state.userFields.email,
            source: this.state.source,
            otp: emailOtp
        }
        this.verifyOTp(dataObj)
    }

    verifyOTp = (dataObj) =>{
        axios.post('/moSignUp/verifyOneTimePassword', dataObj, this.customHeader).then(response => {
            if (response.data.success) {
                this.moveToNext()
            }
        })
    }

    setUserFields = (e) => {
        console.log("print taget name and value o opt : " + e.target.name   + " and " + e.target.value)
        let newUserFields = this.state.userFields
        newUserFields[e.target.name] = e.target.value
        this.setState({
            userFields : newUserFields,
            childProps : this.getPropsForComp()
        })
    }

    render() {
        const { userFields, signupProcess, current, checkUser, enableLoader, childProps } = this.state
        var Current = undefined
        if (!enableLoader) {
            Current = this.state.screens.get(signupProcess)[current]
             console.log('childprops before rendering in setmethod : ' + childProps[Current.name])
          
        }
        const renderElement = Current ?
            <div className="container">
            
                <svg className="back-button" onClick={this.moveToPrevStep} viewBox='0 0 24 24' width='24px' height='24px' xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" >
                    <path fillOpacity='true' d='M21 11H6.83l3.58-3.59L9 6l-6 6 6 6 1.41-1.41L6.83 13H21z' />
                </svg>
                <br/>
                <Current {...childProps[Current.name]}></Current>
            </div>
            :
            <Loader message='Loading...' />
        return (
            renderElement
        )
    }
}

export default Container
