import React, { Component } from 'react'

function EmailInput(props) {

    return (

        <>
            Enter Email: 
            <input name="email" value={props.email} onChange={props.setUserFields}></input>

            <button onClick={props.submitInputEmail}>Next</button>
        </>
    )
}

export default EmailInput