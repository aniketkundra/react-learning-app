import React, { Component } from 'react'

export class EmailVerifier extends Component {

   constructor(props) {
        super(props)
    
        this.state = {
            otp : ''
        }
    }

    setOtp = (e) => {
        this.setState({
            otp : e.target.value
        })
    }
    validateOTP = () => {
        this.props.validateOtpForEmail(this.state.otp)
    }
    render() {

        return (
            <>
            <h2>Please Verify your Email you entered</h2>
            <label> Enter your OTP</label>
            <input name="emailOtp" value={this.state.otp} onChange={this.setOtp}></input>
            <button onClick={this.validateOTP}>Verify OTP</button>
        </>
        )
    }
}

export default EmailVerifier
