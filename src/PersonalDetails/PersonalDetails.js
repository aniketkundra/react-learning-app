import React, { Component } from 'react'

class PersonalDetails extends Component {
    render() {
        return (
            <>
                <h1>Enter Your Personal Details</h1>
                <div>
                    <label>First Name: </label>
                    <input name="firstName" value={this.props.userFields.firstName} onChange={this.props.setUserFields}></input>
                </div>

                <div>
                <label>Last Name:  </label>
                    <input name="lastName" value={this.props.userFields.lastName} onChange={this.props.setUserFields}></input>
                </div>

                <div>
                <label>Phone Number:  </label>
                    <input name="phone" value={this.props.userFields.phone} onChange={this.props.setUserFields}></input>
                </div>
                <button onClick={this.props.moveToNextStep}>verify</button>
            </>
        )
    }
}

export default PersonalDetails
