import React, { Component } from 'react'
import Container from './Container'
import Header from './Header'
import './App.css'

export class RootContainer extends Component {
    render() {
        return (
            <div className= "main-container">
                <Header/>
                <Container/>
            </div>
        )
    }
}

export default RootContainer
