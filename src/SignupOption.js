import React, { Component } from 'react'

function SignupOption(props) {

    return (
        <>
            <h1>Choose your Signup Process</h1>
            <button value="EMAIL" onClick={props.setSignUpProcess}>Email</button>
            <button type='button' value="PHONE" onClick={props.setSignUpProcess}>Phone</button>
        </>
    )
}

export default SignupOption