import React, { Component } from 'react'
import TestChild from './TestChild'

export class Test extends Component {
    
    constructor(props) {
        super(props)
    
        this.state = {
             name:''
        }
    }
    
    handler = (e) => {
        this.setState({
            name: e
        })
    }

    render() {
        return (
            <div>
               <TestChild handle={this.handler}></TestChild>
            </div>
        )
    }
}

export default Test
