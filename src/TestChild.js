import React, { Component } from 'react'

export class TestChild extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             name1: ''
        }
    }
    handlerchild = (e) => {
        this.
      this.props.handle(e.target.value)
    }
    render() {
        return (
            <div>
                 <input value={this.state.name1} onChange={this.handlerchild}></input>
            </div>
        )
    }
}

export default TestChild
